package org.artyom.top;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Search {

    public List<Integer> topN(Stream<Integer> input, int limit) {

        return input
                .parallel()
                .sorted(Comparator.reverseOrder())
                .limit(limit)
                .collect(Collectors.toList());
    }
}
