package org.artyom.top;

import org.junit.Before;
import org.junit.Test;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import static org.junit.Assert.assertTrue;

public class SearchTest {

    private Search search = new Search();
    private int streamSize = 100_000_000;
    private Stream<Integer> testStream;

    @Before
    public void init() {
        testStream = Stream.iterate(0, value -> value + 1).limit(streamSize);
    }

    @Test
    public void testTopN() {

        int resultListSize = 10;
        List<Integer> expectedResultList =  IntStream.rangeClosed(streamSize - resultListSize, streamSize - 1)
                .boxed()
                .collect(Collectors.toList());
        List<Integer> resultList = search.topN(testStream, resultListSize);
        assertTrue(expectedResultList.containsAll(resultList));
    }
}
